package org.abbtech.dto.request;

import lombok.Data;
import org.abbtech.enums.CardType;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class CardRequestDTO {
    private String name;
    private CardType type;
    private String number;
    private int cvv;
    private LocalDate expireDate;
    private BigDecimal balance;
    private String userId;
}
