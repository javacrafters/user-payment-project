package org.abbtech.dto.request;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class TransferRequestDTO {

    private String senderCardNumber;
    private String receiverCardNumber;
    private String categoryId;
    private BigDecimal amount;
    private String currency;
}
