package org.abbtech.dto.request;

import lombok.Data;

    import java.math.BigDecimal;
    import java.time.LocalDate;

    @Data
    public class PaymentRequestDTO {
        private LocalDate date;
        private BigDecimal amount;
        private String currency;
        private String cardNumber;
        private String categoryId;

}
