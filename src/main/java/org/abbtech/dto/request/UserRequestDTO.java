package org.abbtech.dto.request;

import lombok.Data;

@Data
public class UserRequestDTO {
    private String name;
    private String surname;
    private int age;
}
