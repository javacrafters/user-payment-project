package org.abbtech.dto.response;

import lombok.*;
import org.abbtech.enums.CardType;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardResponseDTO {
    private String cardId;
    private String name;
    private CardType type;
    private String number;
    private int cvv;
    private LocalDate expireDate;
    private BigDecimal balance;
    private String userId;
}
