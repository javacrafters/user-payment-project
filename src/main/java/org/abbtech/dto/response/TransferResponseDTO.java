package org.abbtech.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferResponseDTO {
    private String transferId;
    private String transactionId;
    private String sender;
    private String receiver;
    private LocalDateTime transactionDate;
    private String status;

}
