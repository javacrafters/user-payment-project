package org.abbtech.enums;

public enum CardType {
    DEBIT,
    CREDIT
}
