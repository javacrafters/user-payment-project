package org.abbtech.repository;

import org.abbtech.model.PaymentCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PaymentCategoryRepository extends JpaRepository<PaymentCategory, UUID> {
}
