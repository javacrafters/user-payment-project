package org.abbtech.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "transfer")
@AllArgsConstructor
@NoArgsConstructor
public class Transfer {
    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", nullable = false)
    private UUID id;
    @Column(name = "sender_card_number", nullable = false)
    private String senderCardNumber;
    @Column(name = "receiver_card_number", nullable = false)
    private String receiverCardNumber;
    @Column(name = "amount", nullable = false)
    private BigDecimal amount;
    @Column(name = "currency", nullable = false)
    private String currency;
    @Column(name = "transfer_date", nullable = false)
    private LocalDateTime transferDate;
    @Column(name = "transaction_id", nullable = false)
    private String transactionId;



}
