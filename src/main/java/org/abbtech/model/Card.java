package org.abbtech.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.abbtech.enums.CardType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "card", schema = "payment_system")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Card implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")

    private UUID id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private CardType type;  //Debit or Credit

    @Column(name = "card_number", nullable = false, unique = true)
    private String cardNumber;

    @Column(name = "cvv", nullable = false)
    private int cvv;

    @Column(name = "expire_date")
    private LocalDate expireDate;

    @Column(name = "balance")
    private BigDecimal balance;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
