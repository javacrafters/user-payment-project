package org.abbtech.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.model.PaymentCategory;
import org.abbtech.service.PaymentCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/payment-categories")
@RequiredArgsConstructor
public class PaymentCategoryController  {

    private final PaymentCategoryService paymentCategoryService;

    @GetMapping()
    public ResponseEntity<List<PaymentCategory>> getAllPaymentCategories() {
        List<PaymentCategory> categories = paymentCategoryService.getAllPaymentCategories();
        return ResponseEntity.ok(categories);
    }
}
