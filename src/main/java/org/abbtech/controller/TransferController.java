package org.abbtech.controller;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.TransferRequestDTO;
import org.abbtech.dto.response.TransferResponseDTO;
import org.abbtech.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;

    @PostMapping
    public ResponseEntity<TransferResponseDTO> transfer(@RequestBody TransferRequestDTO transferRequestDTO) {
        TransferResponseDTO transferResponseDTO = transferService.transfer(transferRequestDTO);
       //TODO getTransactionId ?
        if (transferResponseDTO != null) {
            return ResponseEntity.ok(transferResponseDTO);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(transferResponseDTO);
        }
    }
}
