package org.abbtech.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.PaymentRequestDTO;
import org.abbtech.dto.response.PaymentResponseDTO;
import org.abbtech.service.PaymentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment")
@RequiredArgsConstructor
public class PaymentController {

    private final PaymentService paymentService;

    @PostMapping
    public ResponseEntity<PaymentResponseDTO> doPayment(@RequestBody PaymentRequestDTO paymentRequestDTO) {
        PaymentResponseDTO paymentResponseDTO = paymentService.doPayment(paymentRequestDTO);
        if (paymentResponseDTO.getTransactionId() != null) {
            return ResponseEntity.ok(paymentResponseDTO);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(paymentResponseDTO);
        }
    }
}
