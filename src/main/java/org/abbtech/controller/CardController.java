package org.abbtech.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.CardRequestDTO;
import org.abbtech.dto.response.CardResponseDTO;
import org.abbtech.model.Card;
import org.abbtech.service.CardService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/cards")
@RequiredArgsConstructor
public class CardController {
    private final CardService cardService;

    @PostMapping("/register")
    public ResponseEntity<CardResponseDTO> registerCard(@RequestBody CardRequestDTO cardRequestDTO) {
        CardResponseDTO card = cardService.registerCard(cardRequestDTO);
        if (card != null) {
            return ResponseEntity.ok(card);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(card);
        }
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<CardResponseDTO>> getAllCardsByUserId(@PathVariable String userId) {
        List<CardResponseDTO> cards = cardService.getAllCardsByUserId(userId);
        return ResponseEntity.ok(cards);
    }
}