package org.abbtech.service;

import org.abbtech.dto.request.PaymentRequestDTO;
import org.abbtech.dto.response.PaymentResponseDTO;

public interface PaymentService {
    PaymentResponseDTO doPayment(PaymentRequestDTO paymentRequestDTO);
}
