package org.abbtech.service;

import org.abbtech.model.PaymentCategory;

import java.util.List;

public interface PaymentCategoryService  {
    List<PaymentCategory> getAllPaymentCategories();
}
