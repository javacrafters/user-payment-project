package org.abbtech.service;

import org.abbtech.dto.request.CardRequestDTO;
import org.abbtech.dto.response.CardResponseDTO;
import org.abbtech.model.Card;

import java.util.List;

public interface CardService {
    CardResponseDTO registerCard(CardRequestDTO cardRequestDTO);
    List<CardResponseDTO> getAllCardsByUserId(String userId);

}
