package org.abbtech.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.model.PaymentCategory;
import org.abbtech.repository.PaymentCategoryRepository;
import org.abbtech.service.PaymentCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PaymentCategoryServiceImpl  implements PaymentCategoryService {
    private final PaymentCategoryRepository paymentCategoryRepository;


    @Override
    public List<PaymentCategory>    getAllPaymentCategories() {
        return paymentCategoryRepository.findAll();
    }
}
