package org.abbtech.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.TransferRequestDTO;
import org.abbtech.dto.response.TransferResponseDTO;
import org.abbtech.model.Card;
import org.abbtech.model.Transfer;
import org.abbtech.repository.CardRepository;
import org.abbtech.repository.TransferRepository;
import org.abbtech.service.TransferService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TransferServiceImpl implements TransferService {

    private final TransferRepository transferRepository;
    private final CardRepository cardRepository;

    @Override
    public TransferResponseDTO transfer(TransferRequestDTO transferRequestDTO) {

        Card senderCard =
                cardRepository.findByCardNumber(transferRequestDTO.getSenderCardNumber())
                        .orElseThrow(() -> new RuntimeException("Sender card not found"));
        Card receiverCard =
                cardRepository.findByCardNumber(transferRequestDTO.getReceiverCardNumber()).orElseThrow();

        BigDecimal transferAmount = transferRequestDTO.getAmount();
        if (senderCard.getBalance().compareTo(transferAmount) >= 0
                && senderCard.getExpireDate().isAfter(ChronoLocalDate.from(LocalDateTime.now()))) {
            senderCard.setBalance(senderCard.getBalance().subtract(transferAmount));
            receiverCard.setBalance(receiverCard.getBalance().add(transferAmount));

            cardRepository.save(senderCard);
            cardRepository.save(receiverCard);

            Transfer transfer = new Transfer();
            transfer.setTransactionId(UUID.randomUUID().toString());
            transfer.setSenderCardNumber(senderCard.getCardNumber());
            transfer.setReceiverCardNumber(receiverCard.getCardNumber());
            transfer.setAmount(transferAmount);
            transfer.setCurrency(transferRequestDTO.getCurrency());
            transfer.setTransferDate(LocalDateTime.now());
            transferRepository.save(transfer);

            return TransferResponseDTO.builder()
                    .transferId(transfer.getId().toString())
                    .transactionId(transfer.getTransactionId())
                    .sender(transfer.getSenderCardNumber())
                    .receiver(transfer.getReceiverCardNumber())
                    .transactionDate(transfer.getTransferDate())
                    .status("Transfer successfully processed.")
                    .build();
        } else {
            String errorMessage = senderCard.getBalance().compareTo(transferRequestDTO.getAmount()) < 0 ?
                    "Insufficient balance on source card!" : "Source card has expired!";
            return new TransferResponseDTO(null, null, null, null, LocalDateTime.now(), errorMessage);
        }
    }
}

