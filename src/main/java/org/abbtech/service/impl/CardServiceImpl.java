package org.abbtech.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.CardRequestDTO;
import org.abbtech.dto.response.CardResponseDTO;
import org.abbtech.model.Card;
import org.abbtech.model.User;
import org.abbtech.repository.CardRepository;
import org.abbtech.service.CardService;
import org.abbtech.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {
    private final CardRepository cardRepository;
    private final UserService userService;


    @Override
    public CardResponseDTO registerCard(CardRequestDTO cardRequestDTO) {
        User user = userService.findById(UUID.fromString(cardRequestDTO.getUserId()));
        Card card = new Card();
        card.setUser(user); // Set the user for the card
        card.setName(cardRequestDTO.getName());
        card.setType(cardRequestDTO.getType());
        card.setCardNumber(cardRequestDTO.getNumber());
        card.setCvv(cardRequestDTO.getCvv());
        card.setExpireDate(cardRequestDTO.getExpireDate());
        card.setBalance(cardRequestDTO.getBalance());

         cardRepository.save(card);

         return CardResponseDTO.builder().cardId(String.valueOf(card.getId())).build();
    }


    @Override
    public List<CardResponseDTO> getAllCardsByUserId(String userId) {
        List<Card> cards = cardRepository.findAllByUserId(UUID.fromString(userId));

        List<CardResponseDTO> cardResponses = new ArrayList<>();
        for (Card card : cards) {
            CardResponseDTO cardResponseDTO = new CardResponseDTO();
            cardResponseDTO.setCardId(String.valueOf(card.getId()));
            cardResponseDTO.setName(card.getName());
            cardResponseDTO.setType(card.getType());
            cardResponseDTO.setNumber(card.getCardNumber());
            cardResponseDTO.setCvv(card.getCvv());
            cardResponseDTO.setExpireDate(card.getExpireDate());
            cardResponseDTO.setBalance(card.getBalance());
            cardResponseDTO.setUserId(userId);
            cardResponses.add(cardResponseDTO);
        }
        return cardResponses;
        // return cardRepository.findAll();//findAllByUserId(UUID.fromString(userId));
    }
}
