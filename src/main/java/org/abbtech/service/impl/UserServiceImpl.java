package org.abbtech.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.UserRequestDTO;
import org.abbtech.dto.response.UserResponseDTO;
import org.abbtech.model.User;
import org.abbtech.repository.UserRepository;
import org.abbtech.service.UserService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserResponseDTO registerUser(UserRequestDTO userRequestDTO) {
        User user = new User();
        user.setName(userRequestDTO.getName());
        user.setSurname(userRequestDTO.getSurname());
        user.setAge(userRequestDTO.getAge());
         userRepository.save(user);
        return UserResponseDTO.builder().userId(String.valueOf(user.getId())).build();
    }

    @Override
    public User findById(UUID id) {
        return userRepository.findById(id).orElse(null);
    }
}
