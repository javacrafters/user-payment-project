package org.abbtech.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.dto.request.PaymentRequestDTO;
import org.abbtech.dto.response.PaymentResponseDTO;
import org.abbtech.model.Card;
import org.abbtech.model.Payment;
import org.abbtech.model.PaymentCategory;
import org.abbtech.repository.CardRepository;
import org.abbtech.repository.PaymentCategoryRepository;
import org.abbtech.repository.PaymentRepository;
import org.abbtech.service.PaymentService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class    PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;
    private final PaymentCategoryRepository paymentCategoryRepository;
    private final CardRepository cardRepository;

    @Override
    public PaymentResponseDTO doPayment(PaymentRequestDTO paymentRequestDTO) {
        PaymentCategory paymentCategory =
                paymentCategoryRepository.findById(UUID.fromString(paymentRequestDTO.getCategoryId()))
                        .orElseThrow();
       Card card =
                cardRepository.findByCardNumber(paymentRequestDTO.getCardNumber())
                        .orElseThrow(() -> new RuntimeException("Card not found"));

            if (card.getBalance().compareTo(paymentRequestDTO.getAmount()) >= 0
                    && card.getExpireDate().isAfter(LocalDate.now())) {
                card.setBalance(card.getBalance().subtract(paymentRequestDTO.getAmount()));
                cardRepository.save(card);

                Payment payment = new Payment();
                payment.setTransactionId(String.valueOf(UUID.randomUUID()));
                payment.setDate(LocalDate.now());
                payment.setAmount(paymentRequestDTO.getAmount());
                payment.setCurrency("AZN");
                payment.setCardNumber(card.getCardNumber());
                payment.setPaymentCategory(paymentCategory.getId().toString());
                paymentRepository.save(payment);
                return new PaymentResponseDTO(payment.getId().toString(),
                        payment.getTransactionId(), payment.getDate(), "Payment successfully processed.");
            } else {
                String errorMessage = card.getBalance().compareTo(paymentRequestDTO.getAmount()) < 0 ?
                        "There is not enough balance on the card!" : "The card has expired!";
                return new PaymentResponseDTO(null,null, LocalDate.now(), errorMessage);
            }

    }

}
