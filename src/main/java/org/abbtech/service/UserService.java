package org.abbtech.service;


import org.abbtech.dto.request.UserRequestDTO;
import org.abbtech.dto.response.UserResponseDTO;
import org.abbtech.model.User;

import java.util.UUID;

public interface UserService  {
    UserResponseDTO registerUser(UserRequestDTO userRequestDTO);
    User findById(UUID id);
}
