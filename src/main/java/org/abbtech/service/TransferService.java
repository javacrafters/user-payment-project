package org.abbtech.service;

import org.abbtech.dto.request.TransferRequestDTO;
import org.abbtech.dto.response.TransferResponseDTO;

public interface TransferService {
    TransferResponseDTO transfer(TransferRequestDTO transferRequestDTO);
}
